### Requirements ###

1. Open the 'config.php'. The location of this file in 'application/config'.
2. Change the value of variable $config['base_url']. This depends on the location of your file.
3. Set RewriteBase on .htaccess according your server configuration
4. Set $config['index_page'] on application/config/config.php as '' (empty string)