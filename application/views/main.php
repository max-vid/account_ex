
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <a class="btn btn-default right" href="<?php echo site_url()?>convmaint"><span class="glyphicon glyphicon-cog"/> マスターメンテナンス</a>
        </div>
      </div>


      <form id="form-conv" method="post" action="conv" enctype="multipart/form-data">

        <div class="row">
            <div class="col-md-3">会計ソフト</div>
            <div class="col-md-6">
                <select name="format_id" class="form-control">
                    <option value="" selected="selected">- 会計ソフトを選択 -</option>
                    <?php foreach($format as $result) echo '<option value="'.$result->id.'">'.$result->name.'</option>'?>
                </select>
            </div>
            <div class="col-md-3"></div>
        </div>

         <div class="row">
            <div class="col-md-3">対象データ</div>
            <div class="col-md-6">
                <select name="datatype" class="form-control">
                    <option value="" selected="selected">- 対象データを選択 -</option>
                    <option value="1">売上データ</option>
                    <option value="2">入金データ</option>
                    <option value="3">出金データ</option>
                </select>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-3">対象期間</div>
            <div class="col-md-6">
              <div class="input-daterange input-group" id="target_date">
                <input type="text" class="input-sm form-control" name="date_start"/>
                <span class="input-group-addon">~</span>
                <input type="text" class="input-sm form-control" name="date_end"/>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button type="submit" id="button-next" class="btn btn-default" disabled="disabled">実行 <span class="glyphicon glyphicon-arrow-right"/></button>
            </div>
        </div>

      </form>
      
    </div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#target_date').datepicker( {
            orientation: "top auto",
            language: "ja"
        });

        formCheck();
        $('#form-conv select,input').change(function() {
            formCheck();
        });


        function formCheck() {
            dataTypeCheck();
            targetDateCheck();
            buttonNextCheck();
        }

        function dataTypeCheck() {
            if( $('select[name=format_id]').val() == "" ) {
                $('select[name=datatype]').prop("disabled",true);
            } else {
                $('select[name=datatype]').prop("disabled",false);
            }
        }

        function targetDateCheck() {
            if( $('select[name=datatype]').val() == ""
                || $('select[name=datatype]').prop("disabled") == true ) {
                $('input[name=date_start]').prop("disabled",true);
                $('input[name=date_end]').prop("disabled",true);
            } else {
                $('input[name=date_start]').prop("disabled",false);
                $('input[name=date_end]').prop("disabled",false);
            }
        }

        function buttonNextCheck() {
            if( $('input[name=date_start]').val() != ""
                && $('input[name=date_start]').prop("disabled") == false
                && $('input[name=date_end]').val() != ""
                && $('input[name=date_end]').prop("disabled") == false ) {
                $('#button-next').prop("disabled",false);
            } else {
                $('#button-next').prop("disabled",true);   
            }
        }
    });
</script>