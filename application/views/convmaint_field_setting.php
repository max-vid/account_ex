<div class="panel-default panel" style="margin: 0px">
  <div class="panel-heading">
    <h3 class="panel-title"> 列 : <?php echo $column_name ?></h3>
  </div>

  <div class="panel body">      
      <div class="row">
        <div class="col-sm-2 row-setting-left">
          <input type="radio" name="field_setting" value="1"
            <?php
              if($field_setting->field_setting == '1' || $field_setting->field_setting == '0') {
                echo 'checked="checked"';
                $fixed_value = $field_setting->field_setting_details;
              } else {
                $fixed_value = '';
              }
            ?>
          > 1. 固定値</input>
        </div>
        <div class="col-sm-3">
          <!--固定値を入力-->
          <input type="text" class="form-control" name="fixed_value" value="<?php echo $fixed_value?>"/>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3 row-setting-left">
          <input type="radio" name="field_setting" value="2"
            <?php
              if($field_setting->field_setting == '2') {
                echo 'checked="checked"';
              }
            ?>
          > 2. 変換元ファイルから移行</input>
        </div>
      </div>

      <div class="row row-setting-sub">
        <div class="col-sm-12 row-setting-left" style="margin: -15px 0px 10px 30px;padding: 0px;width: 95%;overflow-x: scroll">
          <?php echo $origin_table?>
        </div>
      </div>

      <div class="row row-setting-sub">
        <div class="col-sm-3 row-setting-left" style="margin-top: -20px">
          <input type="checkbox" name="convert" 
            <?php 
              if($field_setting->field_setting == '2') {
                $field_setting_details = explode(':',$field_setting->field_setting_details);
                $convert = $field_setting_details[2];
                if($convert == 1) echo 'checked="checked"';
              }
            ?>
          >データを変換する</input>
        </div>
      </div>

      <div class="row row-setting-sub">
        <div class="col-sm-12 row-setting-left" style="margin-top: -20px">
          <?php echo $map_table?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3 row-setting-left">
          <input type="radio" name="field_setting" value="2"
            <?php
              if($field_setting->field_setting == '3') {
                echo 'checked="checked"';
              }
            ?>
          > 3. 条件指定</input>
        </div>
      </div>

      <div class="row row-setting-sub">
        <div class="col-sm-12 row-setting-left" style="margin-top: -20px">
          <?php echo $cond_table?>
        </div>
      </div>

      <div class="row">
          <div class="col-sm-12">
            <button type="button" id="button-cancel" class="btn btn-default">キャンセル</button>
            <button type="button" id="button-reload" class="btn btn-default"><span class="glyphicon glyphicon-refresh"/> 再確認</button>
            <button type="button" id="button-save" class="btn btn-default"><span class="glyphicon glyphicon-floppy-disk"/> 保存</button>
          </div>
      </div>

  </div>
</div>

      <script type="text/javascript">
        $(document).ready(function (){
          var format_id = <?php echo $format_id?>,
              method_id = <?php echo $method_id?>,
              datatype = <?php echo isset($datatype) ? $datatype : 0?>,
              field_setting_id = <?php echo $field_setting_id;?>;
          
          $('#button-save').click(function(){
            $.ajax({
              type: 'POST',
              url: '../field_setting_save',
              data: { format_id: format_id, method_id: method_id,
                field_setting_id: field_setting_id,
                field_setting: $('input[name=field_setting]:checked').val(),
                fixed_value: $('input[name=fixed_value]').val(),
                origin_column: $('input[name=origin_column]:checked').val(),
                convert: $('input[name=convert]').prop('checked')
              },
              dataType: 'HTML',
              error: function(){
                alert("Error");
              },
              success: function(data){
                $('div.container').html(data);
              }
            });
          });

          $('#button-reload').click(function(){
            $.ajax({
              type: 'POST',
              url: '../field_setting',
              data: { format_id: format_id, method_id: method_id, datatype: datatype,
                field_setting_id: field_setting_id
              },
              dataType: 'HTML',
              error: function(){
                alert("Error");
              },
              success: function(data){
                $('div.container').html(data);
              }
            });
          });

          $('#button-cancel').click(function(){
            $.ajax({
              type: 'GET',
              url: '../method_details/' + method_id,
              dataType: 'HTML',
              error: function(){
                alert("Error");
              },
              success: function(data){
                $('div.container').html(data);
              }
            });
          });
        });
      </script>
