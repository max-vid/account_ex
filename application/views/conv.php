<?php
    if (!isset($reload)) { ?>
<div class="panel-default panel" id="output_preview" style="margin: 0px">
<?php
    } ?>

    <div class="panel-heading">
        <h3 class="panel-title">Output Preview</h3>
    </div>

    <div class="panel body">
    	<div class="row row">
            <div class="col-sm-12 row-setting-left" style="margin: 10px 0px 10px 30px;padding: 0px;width: 95%; height: 50vh;overflow: scroll">
                <?php echo $preview_table ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button type="button" id="button-back" class="btn btn-default">
                	<span class="glyphicon glyphicon-arrow-left"/> 戻る
                </button>
                <button type="button" id="button-reload" class="btn btn-default">
                	<span class="glyphicon glyphicon-refresh"/> 再確認
                </button>
                <form id="form-export" method="post" action="conv/export_csv" enctype="multipart/form-data">
                    <input type="hidden" name="method_id" value="<?php echo $method_id?>">
                    <input type="hidden" name="date_start" value="<?php echo $date_start?>">
                	<input type="hidden" name="date_end" value="<?php echo $date_end?>">
                	<button type="submit" id="button-download" class="btn btn-default">
                		<span class="glyphicon glyphicon-download-alt"/> CSVダウンロード
                	</button>
                </form>
            </div>
        </div>

    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        var format_id = <?php echo $format_id?>,
            date_start = <?php echo "'".$date_start."'"?>,
            date_end = <?php echo "'".$date_end."'"?>;

        $('#button-back').click(function(){
            history.go(-1);
        });

        $('#button-reload').click(function(){
            $.ajax({
                type: 'POST',
                url: 'conv',
                data: { format_id: format_id, date_start: date_start, date_end: date_end, reload: 1 },
                dataType: 'HTML',
                error: function(){
                    alert("Error");
                },
                success: function(data){
                    $('#output_preview').html(data);
                }
            });
        });
    });
    </script>
    
<?php
    if (!isset($reload)) { ?>
</div>
<?php
    } ?>