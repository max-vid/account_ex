<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="<?php echo DEFAULT_CHAR_ENCODING?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VID</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url()."public/css/bootstrap/"?>bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url()."public/css/bootstrap/"?>bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url()."public/css/bootstrap/"?>bootstrap-custom.css"/>
    <link rel="stylesheet" href="<?php echo base_url()."assets/bootstrap-datepicker/"?>bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url()."public/css/"?>common.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url()."public/js/"?>jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."public/js/"?>moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."public/js/"?>bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."public/js/"?>bootstrap/collapse.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."public/js/"?>bootstrap/transition.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/bootstrap-datepicker/"?>bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/bootstrap-datepicker/"?>bootstrap-datepicker.ja.js"></script>
  </head>
  <body>