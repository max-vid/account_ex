<?php
if (!isset($reload)) {
  foreach($css_files as $file): ?>
  <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />

  <?php endforeach; ?>
  <?php foreach($js_files as $file): ?>

    <script src="<?php echo $file; ?>"></script>
  <?php endforeach;
} ?>

<div class="container">
  <a href="<?php echo base_url() ?>">メイン画面</a> ->
  <a href="<?php echo site_url('convmaint') ?>">マスター管理</a> ->
  <a href="<?php echo site_url('convmaint/method').'/'.$format->id?>"><?php echo $format->name; ?></a> ->
  <a href="<?php echo site_url('convmaint/method_details').'/'.$method->id?>"><?php echo $method->name; ?></a>
  <br/>
  <?php echo $output; ?>
</div>

<script type="text/javascript">
$(document).ready(function(){
  var format_id = <?php echo $format->id?>,
      method_id = <?php echo $method->id?>,
      datatype = <?php echo isset($datatype) ? $datatype: 0 ?>;

  $(document).on("click", "a.edit-icon", function(event) {
    event.preventDefault();
    var field_setting_id = $(this).attr('href').substr($(this).attr('href').indexOf('#') + 1);
    $.ajax({
      type: 'POST',
      url: '../field_setting',
      data: { format_id: format_id, method_id: method_id, datatype: datatype,
        field_setting_id: field_setting_id
      },
      dataType: 'HTML',
      error: function(){
        alert("Error");
      },
      success: function(data){
        $('div.container').html(data);
      }
    });
  });
});
</script>