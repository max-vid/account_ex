<div class="panel-default panel" style="margin: 0px">
	<div class="panel-heading">
		<h3 class="panel-title">新規作成</h3>
    </div>

    <div class="panel body">

	<form id="form-conv" method="post" action="../convmaint/add_cond_commit" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-3">条件名</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="cond_name"/>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="row">
			<div class="col-sm-3">優先順位</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="cond_priority"/>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
			        <li class="active"><a href="#condition" data-toggle="tab">条件</a></li>
			        <li><a href="#value" data-toggle="tab">設定値</a></li>
			    </ul>
			    <div id="my-tab-content" class="tab-content" style="padding-left: 20px">

			        <div class="tab-pane active" id="condition">
			        	<div class="row condition_group" id="condition_1">
				            <div class="col-sm-9">
						        <div class="panel panel-default" style="margin-top: 10px;margin-bottom: 0px">
							        <div class="row">
							            <div class="col-sm-3">対象</div>
							            <div class="col-sm-8">
							            	<select name="column_id[1]" class="form-control">
							                    <option value="" selected="selected">- 対象 -</option>
							                </select>
							            </div>
							        </div>

							        <div class="row">
							            <div class="col-sm-3">条件</div>
							            <div class="col-sm-8">
							            	<select name="condition_type[1]" class="form-control">
							                    <option value="" selected="selected">- 条件 -</option>
							                    <option value="1">と等しい</option>
							                    <option value="2">より大きい</option>
							                    <option value="3">より小さい</option>
							                    <option value="4">以上</option>
							                    <option value="5">以下</option>
							                </select>
							            </div>
							        </div>
						       	</div>
					        </div>
				        	
				            <div class="col-sm-3">
				            	<button type="button" id="button-add-group" class="btn btn-default right">
				            		<span class="glyphicon glyphicon-plus"/> グループ追加
				            	</button>
				            </div>
			        	</div>

			        </div>

			        <div class="tab-pane" id="value">
			        	<br/>
			            <div class="row">
			            	<div class="col-sm-3">設定値</div>
			            	<div class="col-sm-6">
				            	<div class="input-group">
				                    <span class="input-group-addon">
				                        <input type="radio" name="value_type" value="fixed">固定値</input>
				                    </span>
				                    <input type="text" name="fixed_value" class="form-control" value="">
				                </div>
			            	</div>
			            </div>

			            <div class="row">
			            	<div class="col-sm-3"></div>
			            	<div class="col-sm-6">
				            	<div class="input-group">
				                    <span class="input-group-addon">
				                        <input type="radio" name="value_type" value="condition">項目を指定</input>
				                    </span>
				                    <select name="condition_id" class="form-control">
				                        <option value="" selected="selected">- 条件 -</option>
				                    </select>
				                </div>
				            </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

		<div class="row">
            <div class="col-sm-12">
                <button type="submit" id="button-save" class="btn btn-default">
                	<span class="glyphicon glyphicon-floppy-disk"/> 保存
                </button>
            </div>
        </div>
	</form>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#button-save").click(function() {
		alert("Save conditional");
	});

	$("#button-add-group").click(function() {
		addCondition();
	});

	$(document).on("click", ".btn-remove-cond", function() {
		var group_id = $(this).closest('.condition_group').prop('id');
		$('#' + group_id).remove();
	});

	function addCondition() {
		var group_id = parseInt($('#condition').children('.condition_group').last().prop('id').substr(10)) + 1;
		html = '\
		<div class="row condition_group" id="condition_'+ group_id +'">\
    		<div class="col-sm-9" style="padding: 0px">\
            	<input type="radio" name="group_cond['+ group_id +']" value="and" checked="checked">かつ</input>\
            	<input type="radio" name="group_cond['+ group_id +']" value="or"> または</input>\
            </div>\
    		<div class="col-sm-9">\
		        <div class="panel panel-default">\
			        <div class="row">\
			            <div class="col-sm-3">対象</div>\
			            <div class="col-sm-8">\
			            	<select name="column_id['+ group_id +']" class="form-control">\
			                    <option value="" selected="selected">- 対象 -</option>\
			                </select>\
			            </div>\
			            <div class="col-sm-1">\
			            	<button type="button" class="btn-remove-cond btn btn-default btn-xs" style="margin: 0px">\
			            		<span class="glyphicon glyphicon-remove"/>\
			            	</button>\
			            </div>\
			        </div>\
			        <div class="row">\
			            <div class="col-sm-3">条件</div>\
			            <div class="col-sm-8">\
			            	<select name="condition_type['+ group_id +']" class="form-control">\
			                    <option value="" selected="selected">- 条件 -</option>\
			                    <option value="1">と等しい</option>\
			                    <option value="2">より大きい</option>\
			                    <option value="3">より小さい</option>\
			                    <option value="4">以上</option>\
			                    <option value="5">以下</option>\
			                </select>\
			            </div>\
			        </div>\
		       	</div>\
	        </div>\
    	</div>';
    	$("#condition").append(html);
	}
});
</script>