
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
    <div class="container">
      <a href="<?php echo base_url() ?>">メイン画面</a> ->
      <a href="<?php echo site_url('convmaint') ?>">マスター管理</a> ->
      <a href="<?php echo site_url('convmaint/column').'/'.$format->id ?>"><?php echo $format->name; ?></a> ->
      <?php echo $column->name.'('.$column->description.')' ?>
      <?php echo $output; ?>
    </div>