<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conv extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("Conv_Model");
        $this->load->model("Vid_Model");
    }

	public function index() {
		$format_id = $_POST["format_id"];
		$datatype = $_POST["datatype"];
		//$date_start = $_POST["date_start"];
		//$date_end = $_POST["date_end"];
		$date_start = $date_end = "";

		$default_method = $this->Conv_Model->getDefaultMethod($format_id, $datatype);
		
		$method_id = $default_method->id;		

		$output["format_id"] = $format_id;
		$output["method_id"] = $method_id;
		$output["datatype"] = $datatype;
		$output["date_start"] = $date_start;
		$output["date_end"] = $date_end;
		$preview = $this->create_preview($method_id, $date_start, $date_end);

		$row_count = count($preview);
		$col_count = count($preview[0]);

		$preview_table = '
				<table class="table-striped table-bordered table">
					<tr>';
		foreach($preview[0] as $header){
			$preview_table .= '<th>'.$header.'</th>';
		}
		$preview_table .=  '
					</tr>
					';
		for($row = 1; $row < $row_count; $row++){
			$preview_table .= '<tr>';
			for($col = 0; $col < $col_count; $col++){
				if('!err' == substr($preview[$row][$col],0,4)){
					$preview_table .= '<td class="out-error">';
					$parse_data = ltrim(explode('!',$preview[$row][$col])[1], 'err:');
					$err_data[explode('=',$parse_data)[0]] = explode('=',$parse_data)[1];
					$value = substr($preview[$row][$col],strrpos($preview[$row][$col],'!')+1);
					$atts = array(
		              'width'      => '800',
		              'height'     => '600',
		              'scrollbars' => 'yes',
		              'status'     => 'yes',
		              'resizable'  => 'yes',
		              'screenx'	   =>  '\'+((parseInt(screen.width) - 800)/2)+\'',
		              'screeny'    =>  '\'+((parseInt(screen.height) - 600)/2)+\'',
		            );
		            if($value == ""){
		            	$value = "-";
		            }
					$preview_table .= anchor_popup('convmaint/mapping/'.$_POST['format_id'].'/'.$err_data['id'], $value, $atts);
					$preview_table .= '</td>';
				}
				else{
					$preview_table .='<td>'.$preview[$row][$col].'</td>';
				}
			}
			$preview_table .= '</tr>';
		}
		$preview_table .=  '
				 </table>';
		$output["preview_table"] = $preview_table;

		if (isset($_POST["reload"])) {
			$output["reload"] = $_POST["reload"];
			$this->load->view('conv',$output);
		} else {
			$this->load->view('templates/header');
			$this->load->view('conv',$output);
			$this->load->view('templates/footer');
		}
	}

	public function create_preview($method_id, $date_start, $date_end) {
		$char_encoding = DEFAULT_CHAR_ENCODING;
		$method = $this->Conv_Model->getMethodDetails($method_id);

		$vidOutputFormat = $this->Vid_Model->getOutputFormat();
		$vidOutput = $this->Vid_Model->getOutputBetweenDate($method->datatype, $date_start, $date_end);

		$of_name = array();
		$of_value = array();

		foreach ($vidOutputFormat as $column) {
			$column_name = $column->column_name;
			$column_name = mb_convert_encoding($column_name, DEFAULT_CHAR_ENCODING, mb_detect_encoding($column_name, CHAR_ENCODING));
			$of_name[$column->id]  = $column_name;
			$of_value[$column->id] = array();
		}

		foreach ($vidOutput as $rowData) {
			$col = 1;
			foreach ($rowData as $originData) {
				$originData = mb_convert_encoding($originData, DEFAULT_CHAR_ENCODING, mb_detect_encoding($originData, CHAR_ENCODING));
				array_push($of_value[$col], $originData);
				$col++;
			}
		}
		$row_count = sizeof($vidOutput);

		$preview = array();
		$preview_header = array();
		$field_setting = $this->Conv_Model->getMethodDetailsList($method_id);
		$col = 1;
		foreach($field_setting as $fs){
			$column_name = $this->Conv_Model->getColumnName($fs->column_id);
			$preview_header[$col] = $column_name;
			if($fs->field_setting == 1){
				$preview[$col] = array();
				for($row = 1; $row <= $row_count; $row++){
					array_push($preview[$col], $fs->field_setting_details);
				}
			}
			else if($fs->field_setting == 2){
				$preview[$col] = array();
				$field_setting_details = explode(':',$fs->field_setting_details);
				$of_id = $field_setting_details[0];
				($field_setting_details[2] == 1) ? $convert = true : $convert = false;
				if($convert){
					foreach ($of_value[$of_id] as $source_value) {
						$map = $this->Conv_Model->mapData($method->format_id,$fs->column_id,$source_value);
						if($map){
							array_push($preview[$col], $map[0]->destination);
						}
						else{
							$map = $this->Conv_Model->mapData($method->format_id,$fs->column_id,'*');
							if($map){
								array_push($preview[$col], $map[0]->destination);		
							}
							else{
								array_push($preview[$col], '!err:id='.$fs->column_id.'!'.$source_value);
							}
						}
					}
				}
				else{
					foreach($of_value[$of_id] as $source_value){
						array_push($preview[$col], $source_value);
					}
				}
			}
			$col++;
		}

		$preview_table = array();
		$preview_table[0] = array();
		foreach($preview_header as $col => $header){
			array_push($preview_table[0], $header);
		}
		for($row = 1; $row <= $row_count; $row++){
			$preview_table[$row] = array();
			for($col = 1; $col <= count($preview_header); $col++){
				if(isset($preview[$col])){
					array_push($preview_table[$row], $preview[$col][$row-1]);
				}
				else{
					array_push($preview_table[$row], '');
				}
			}
		}
		return $preview_table;
	}

	public function export_csv() {
		$table_data = $this->create_preview($_POST["method_id"], $_POST["date_start"], $_POST["date_end"]);

		$method = $this->Conv_Model->getMethodDetails($_POST["method_id"]);
		$tmp_dir = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']).'assets/temp/';
		$file_export = $tmp_dir.'export-'.$method->name.'.csv';
		$output["file_export"] = $file_export;

        $file = fopen($file_export, 'w');
        foreach($table_data as $line){
        	$unmasked_line = array();
        	foreach($line as $cell){
        		if('!err' == substr($cell,0,4)){
        			array_push($unmasked_line, substr($cell,strrpos($cell,'!')+1));
        		}
        		else{
        			array_push($unmasked_line, $cell);
        		}
        	}
        	fputcsv($file, $unmasked_line);
        }
        fclose($file);

    	$this->load->view('templates/header');
		$this->load->view('conv_export_csv',$output);
		$this->load->view('templates/footer');
	}

	public function export_download() {
		$file_export = $_POST["file_export"];
		header('Content-Description: File Transfer');
    	header('Content-Type: application/octet-stream;charset='.DEFAULT_CHAR_SET);
    	header('Content-Disposition: attachment; filename='.basename($file_export));
    	header('Content-Length: ' . filesize($file_export));
    	readfile($file_export);
    	unlink($file_export);
	}
}

/* End of file conv.php */
/* Location: ./application/controllers/conv.php */