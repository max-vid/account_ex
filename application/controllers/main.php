<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model("Conv_Model");
    }

	public function index(){
		$output["format"] = $this->Conv_Model->getAllFormat();
		$this->load->view('templates/header');
		$this->load->view('main', $output);
		$this->load->view('templates/footer');
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */