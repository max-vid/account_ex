<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConvMaint extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('grocery_CRUD');
        $this->load->model("ConvMaint_Model");
        $this->load->model("Vid_Model");
    }

	public function index(){
		$crud = new grocery_CRUD();
		$crud->set_table('conv_format')
			->set_subject('変換フォーマット')
			->columns('name')
			->display_as('name','変換フォーマット')
			->unset_delete()
			->unset_export()
			->unset_print()
			->set_language("japanese")
			->add_action('列を編集する','',site_url('/convmaint/column').'/','plus-icon')
			->callback_column('name',array($this,'_callback_convmaint_method'));
		$output = $crud->render();
		$this->load->view('templates/header');
		$this->load->view('convmaint',$output);
		$this->load->view('templates/footer');
	}

	public function _callback_convmaint_method($value, $row){
		return "<a href='".site_url('convmaint/method/'.$row->id)."'>$value</a>";
	}

	public function method($format_id){
		$crud = new grocery_CRUD();
		$crud->where("format_id",$format_id)
			->set_table('conv_method')
			->set_subject('変換セット')
			->columns('name','description','datatype','default')
			->display_as('name','変換セット')
			->display_as('description','説明')
			->display_as('datatype','データ区分')
			->display_as('default','デフォルト')
			->fields('format_id','name','description','datatype','default')
			->field_type('format_id','hidden',$format_id)
			->field_type('datatype','dropdown', array('1' => '売上データ', '2' => '入金データ', '3' => '出金データ'))
			->field_type('default','true_false')
			->callback_column('name',array($this,'_callback_convmaint_method_details'))
			->callback_column('default',array($this,'_callback_convmaint_method_default'))
			->callback_after_insert(array($this, '_callback_init_method_details'))
			->callback_after_delete(array($this, '_callback_clear_method_details'))
			->unset_export()
			->unset_print()
			->set_language("japanese");
		$output = $crud->render();
		$output->format = $this->ConvMaint_Model->getFormatDetails($format_id);
		$this->load->view('templates/header');
		$this->load->view('convmaint_method',$output);
		$this->load->view('templates/footer');
	}

	public function _callback_convmaint_method_details($value, $row){
		return "<a href='".site_url('convmaint/method_details/'.$row->id)."'>$value</a>";
	}

	public function _callback_convmaint_method_default($value, $row){
		$icon = $value ? "glyphicon-ok" : "glyphicon-minus";
		return '<span class="glyphicon '.$icon.'"/>';
	}

	public function _callback_init_method_details($post_array,$primary_key) {
		if (! $this->ConvMaint_Model->methodDetailsExist($primary_key)) {
			$this->ConvMaint_Model->initMethodDetails($primary_key);
		}
	}

	public function _callback_clear_method_details($primary_key) {
		$this->ConvMaint_Model->clearMethodDetails($primary_key);
	}

	public function method_details($method_id){
		$output = $this->method_details_render($method_id);
		$output->method = $this->ConvMaint_Model->getMethodDetails($method_id);
		$output->format = $this->ConvMaint_Model->getFormatDetails($output->method->format_id);
		$output->datatype = $output->method->datatype;
		$this->load->view('templates/header');
		$this->load->view('convmaint_method_details',$output);
		$this->load->view('templates/footer');
	}

	public function method_details_render($method_id) {
		$crud = new grocery_CRUD();
		$crud->where("method_id",$method_id)
			->set_table('conv_method_details')
			->set_subject('列')
			->columns('column_id','field_setting','field_setting_details')
			->display_as('column_id','変換先項目名')
			->display_as('field_setting','変換設定内容')
			->display_as('field_setting_details','設定値 / 変換有無')
			->callback_column('column_id',array($this,'_callback_convmaint_method_details_column'))
			->callback_column('field_setting',array($this,'_callback_convmaint_method_details_field_setting'))
			->callback_column('field_setting_details',array($this,'_callback_convmaint_method_details_field_setting_details'))
			->add_action('編集','',site_url('convmaint/method_details').'/'.$method_id.'#','edit-icon')
			->unset_operations()
			->set_language("japanese");
		return $crud->render();
	}

	public function _callback_convmaint_method_details_column($value, $row){
		$column = $this->ConvMaint_Model->getColumnDetails($value);
		return $column->name;
	}

	public function _callback_convmaint_method_details_field_setting($value, $row){
		if($value == 0) return '-';
		else if($value == 1) return '固定値';
		else if($value == 2) return '変換元ファイルから移行';
		else return $value;
	}

	public function _callback_convmaint_method_details_field_setting_details($value, $row){
		if($value != '' && strpos($value, ':') !== FALSE){
			$details = explode(':', $value);
			if($details[2] == '0'){
				$convert = '無';
			}
			else if($details[2] == '1'){
				$convert = '有';
			}
			return $details[1].' / '.$convert;
		}
		else{
			return $value;
		}
	}

	public function field_setting(){
		$output = array();
		$output["format_id"] = $_POST["format_id"];
		$output["method_id"] = $_POST["method_id"];
		$output["datatype"] = $_POST["datatype"];
		$output["field_setting_id"] = $_POST["field_setting_id"];

		$field_setting = $this->ConvMaint_Model->getFieldSetting($_POST["field_setting_id"]);
		$method = $this->ConvMaint_Model->getMethodDetails($field_setting->method_id);
		$output["field_setting"] = $field_setting;

		$column = $this->ConvMaint_Model->getColumnDetails($field_setting->column_id);
		$output["column_name"] = $column->name;

		$origin_table = '';
		$vidOutputFormat = $this->Vid_Model->getOutputFormat();
		$vidOutput = $this->Vid_Model->getOutput($_POST["datatype"]);
		$char_encoding = DEFAULT_CHAR_ENCODING;

		if ($vidOutput == null) {
			$origin_table = '返り値が空でした(行数0)';

		} else {
			$origin_table = '
				<table class="table-striped table-bordered">
					<tr>';

			$field_setting_details = explode(':', $field_setting->field_setting_details);
			$selected_column = $field_setting_details[0];
			foreach ($vidOutputFormat as $column) {
				$origin_table .= '
						<div class="btn-group" data-toggle="buttons">
							<th>
								<label class="btn">
									<input type="radio" name="origin_column" value="'.$column->id.':'.$column->column_name.'" ';
				if($column->id == $selected_column){
					$origin_table .= 'checked="checked"';
				}
				$origin_table .=	'/> '.$column->column_name.'
								</label>
							</th>
						</div>';
			}
			$origin_table .= '
					</tr>
					<tr>';

			foreach ($vidOutput as $rowData) {
				foreach ($rowData as $originData) {
					$originData = mb_convert_encoding($originData, DEFAULT_CHAR_ENCODING, mb_detect_encoding($originData, CHAR_ENCODING));
					$origin_table .= '
						<td>'.$originData.'</td>';
				}
				$origin_table .= '
					</tr>';
			}
			$origin_table .= '
				</table>';
		}
		$output["origin_table"] = $origin_table;

		$atts = array(
		              'width'      => '800',
		              'height'     => '600',
		              'scrollbars' => 'yes',
		              'status'     => 'yes',
		              'resizable'  => 'yes',
		              'screenx'	   =>  '\'+((parseInt(screen.width) - 800)/2)+\'',
		              'screeny'    =>  '\'+((parseInt(screen.height) - 600)/2)+\'',
		            );
		$map_table = anchor_popup('convmaint/mapping/'.$method->format_id.'/'.$field_setting->column_id,
				"<button type='button' class='btn' style='margin-bottom: 5px'>
					<span class='glyphicon glyphicon-edit'/> 変換マッピングを編集する
				 </button>", $atts);
		$map_table .= "<br/>";
		$map_list = $this->ConvMaint_Model->getMapList($method->format_id,$field_setting->column_id);
		if(empty($map_list)){
			$map_table .= "変換データがありません";
		}
		else{
			$map_table .= '
					<table class="table-striped table-bordered">
					  <thead>
						<tr>
						  <th>変換元</th>
						  <th>備考</th>
						  <th>変換後</th>
						</tr>
					  </thead>
					  <tbody>';
			foreach($map_list as $map){
				$map_table .= '
						<tr>
						  <td>'.$map->source.'</td>
						  <td>'.$map->explanation_text.'</td>
						  <td>'.$map->destination.'</td>
						 </tr>';
			}
			$map_table .= '
					  </tbody>
					</table>';
		}
		$output["map_table"] = $map_table;

		$atts = array(
		              'width'      => '800',
		              'height'     => '600',
		              'scrollbars' => 'yes',
		              'status'     => 'yes',
		              'resizable'  => 'yes',
		              'screenx'	   =>  '\'+((parseInt(screen.width) - 800)/2)+\'',
		              'screeny'    =>  '\'+((parseInt(screen.height) - 600)/2)+\'',
		            );
		$cond_table = anchor_popup('convmaint/add_cond',
			"<button type='button' class='btn' style='margin-bottom: 5px'>
				<span class='glyphicon glyphicon-plus'/> 新規作成
			 </button>", $atts);
		$cond_table .= "<br/>";
		$cond_list = $this->ConvMaint_Model->getCondList($method->format_id);
		if(empty($cond_list)){
			$cond_table .= "変換データがありません";
		}
		else{
			$cond_table .= '
					<table class="table-striped table-bordered">
					  <thead>
						<tr>
						  <th>優先順位</th>
						  <th>条件名</th>
						  <th>ボタン</th>
						</tr>
					  </thead>
					  <tbody>';
			foreach($cond_list as $cond){
				$cond_table .= '
						<tr>
						  <td>'.$cond->priority.'</td>
						  <td>'.$cond->name.'</td>
						  <td></td>
						 </tr>';
			}
			$cond_table .= '
					  </tbody>
					</table>';
		}
		$output["cond_table"] = $cond_table;

		$this->load->view('convmaint_field_setting',$output);
	}

	public function field_setting_save(){
		if($_POST["field_setting"] == '1'){
			$data = array('field_setting' => $_POST["field_setting"], 'field_setting_details' => $_POST["fixed_value"]);
			$this->ConvMaint_Model->setFieldSetting($_POST["field_setting_id"],$data);
		}
		else if($_POST["field_setting"] == '2'){
			($_POST["convert"] == 'true') ? $convert = 1 : $convert = 0;
			$data = array('field_setting' => $_POST["field_setting"], 'field_setting_details' => $_POST["origin_column"].':'.$convert);
			$this->ConvMaint_Model->setFieldSetting($_POST["field_setting_id"],$data);
		}
		$output = $this->method_details_render($_POST["method_id"]);
		$output->reload = 1;
		$output->method = $this->ConvMaint_Model->getMethodDetails($_POST["method_id"]);
		$output->format = $this->ConvMaint_Model->getFormatDetails($output->method->format_id);
		$this->load->view('convmaint_method_details',$output);
	}

	public function column($format_id){
		$crud = new grocery_CRUD();
		$crud->where("format_id",$format_id)
			->set_table('conv_column')
			->set_subject('列')
			->columns('name','description')
			->display_as('name','列')
			->display_as('description','説明')
			->fields('format_id','name','description')
			->field_type('format_id','hidden',$format_id)
			->set_language("japanese")
			->add_action('変換方法を編集','',site_url('/convmaint/mapping').'/'.$format_id.'/','plus-icon');
		$output = $crud->render();
		$output->format = $this->ConvMaint_Model->getFormatDetails($format_id);
		$this->load->view('templates/header');
		$this->load->view('convmaint_column',$output);
		$this->load->view('templates/footer');
	}

	public function mapping($format_id,$column_id){
		$crud = new grocery_CRUD();
		$crud->where("format_id",$format_id)
			->where("column_id",$column_id)
			->set_table('conv_mapping')
			->set_subject('変換方法')
			->columns('source','destination','explanation_text')
			->display_as('source','変換元')
			->display_as('destination','変換後')
			->display_as('explanation_text','備考')
			->fields('format_id','column_id','source','explanation_text','destination')
			->field_type('format_id','hidden',$format_id)
			->field_type('column_id','hidden',$column_id)
			->set_language("japanese");
		$output = $crud->render();
		$output->format = $this->ConvMaint_Model->getFormatDetails($format_id);
		$output->column = $this->ConvMaint_Model->getColumnDetails($column_id);;
		$this->load->view('templates/header');
		$this->load->view('convmaint_mapping',$output);
		$this->load->view('templates/footer');
	}

	public function add_cond() {
		$this->load->view('templates/header');
		$this->load->view('convmaint_add_cond');
		$this->load->view('templates/footer');
	}
}

/* End of file convmaint.php */
/* Location: ./application/controllers/convmaint.php */