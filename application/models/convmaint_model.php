<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ConvMaint_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function getFormatDetails($format_id){
		$this->db->where('id',$format_id);
		$query = $this->db->get('conv_format');
		return $query->result()[0];
	}

	public function getColumnDetails($column_id){
		$this->db->where('id',$column_id);
		$query = $this->db->get('conv_column');
		return $query->result()[0];
	}

	public function getMethodDetails($method_id){
		$this->db->where('id',$method_id);
		$query = $this->db->get('conv_method');
		return $query->result()[0];
	}

	public function methodDetailsExist($method_id){
		$this->db->where('method_id',$method_id);
		$query = $this->db->get('conv_method_details');
		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function initMethodDetails($method_id){
		$method = $this->getMethodDetails($method_id);
		$format_id = $method->format_id;
		
		$this->db->where("format_id",$format_id);
		$this->db->order_by("id","asc");
		$query = $this->db->get('conv_column');
		$columns = $query->result();

		foreach ($columns as $column) {
			$data = array(
				'method_id' => $method_id,
				'column_id' => $column->id,
				'field_setting' => 0
			);
			$this->db->insert('conv_method_details', $data);
		}
	}

	public function clearMethodDetails($method_id){
		$this->db->where("method_id",$method_id);
		$this->db->delete('conv_method_details');
	}

	public function getFieldSetting($method_details_id){
		$this->db->where('id',$method_details_id);
		$query = $this->db->get('conv_method_details');
		return $query->result()[0];
	}

	public function setFieldSetting($id,$data){
		$this->db->where("id",$id);
		$this->db->update('conv_method_details',$data);
	}

	public function getMapList($format_id,$column_id){
		$this->db->where("format_id",$format_id);
		$this->db->where("column_id",$column_id);
		$query = $this->db->get('conv_mapping');
		return $query->result();
	}

	public function getCondList($format_id) {
		$this->db->where("format_id",$format_id);
		$query = $this->db->get('conv_conditional');
		return $query->result();
	}
}

/* End of file convmaint_model.php */
/* Location: ./application/model/convmaint_model.php */