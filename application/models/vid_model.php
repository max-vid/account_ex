<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vid_Model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db_vid = $this->load->database('vid', true);
	}

	public function getOutputFormat() {
		$query = $this->db_vid->get('output_format');
		return $query->result();
	}
	
	public function getOutput($datatype) {
		switch ($datatype) {
			case 1:
				$output = $this->getSalesData("", "");
				break;
			
			case 2:
				$output = $this->getInData("", "");
				break;

			case 3:
				$output = $this->getOutData("", "");
				break;

			default:
				$output = null;
				break;
		}
		return $output;
	}

	public function getOutputBetweenDate($datatype, $date_start, $date_end) {
		switch ($datatype) {
			case 1:
				$output = $this->getSalesData($date_start, $date_end);
				break;

			case 2:
				$output = $this->getInData($date_start, $date_end);
				break;

			case 3:
				$output = $this->getOutData($date_start, $date_end);
				break;

			default:
				$output = null;
				break;
		}
		return $output;
	}

	public function getSalesData($date_start, $date_end) {
		$sql = "
			#現金支払
			(
				select 
						sl.store_cd	as	店舗コード
				,		store_name	as	店舗名
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	年月日	
				,		'現金支払'	as	売上項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.receipt_amount,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.receipt_amount,0),0)),0)	as	金額	
				,		''	as	備考	
				from		d_sales	sl		
				inner join		m_store	s		
				on		sl.store_cd	=	s.store_cd	
				where "; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
					store_history_date >= " . $date_start ." 
					and store_history_date <= ". $date_end . "
					and ";
		}
		$sql .= "
					sl.del_flg	=	0	
					and		sl.receipt_amount	>	0	
			)

			union

			#クレジット
			(						
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'クレジット支払'	as	項目	
				,		credit_cd	as	クレジットコード	
				,		'' /*credit_ticket_name*/	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
					(					
						select					
								sl.store_cd			
						,		sl.credit_cd			
						,		store_history_date			
						,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.receipt_credit,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.receipt_credit,0),0)),0)	as	amount	
						from		d_sales	sl		
						where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "	
						sl.del_flg	=	0	
						and		sl.receipt_credit	>	0	
						group by		sl.store_cd, store_history_date, sl.credit_cd			
					) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				/*
				left join		m_credit_ticket	ct		
				on		A.store_cd	=	ct.store_cd	
				and		A.credit_cd	=	ct.credit_ticket_cd	
				*/
				where
					/*
					ct.credit_ticket_div	=	1
					and
					*/
					amount	>	0	
			)

			union

			#商品券支払					
			(		
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'商品券支払'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		ticket_cd	as	商品券コード	
				,		'' /*credit_ticket_name*/	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		sl.ticket_cd			
					,		store_history_date			
					,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.receipt_ticket,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.receipt_ticket,0),0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "
						sl.del_flg	=	0	
						and		sl.receipt_ticket	>	0	
					group by		sl.store_cd, store_history_date, sl.ticket_cd			
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				/*
				left join		m_credit_ticket	ct		
				on		A.store_cd	=	ct.store_cd	
				and		A.ticket_cd	=	ct.credit_ticket_cd	
				*/
				where
					/*
					ct.credit_ticket_div	=	2
					and
					*/
					amount	>	0	
			)

			union

			#デビット支払
			(					
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'デビット支払'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		store_history_date			
					,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.receipt_money_ticket2,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.receipt_money_ticket2,0),0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "
						sl.del_flg	=	0	
						and		sl.receipt_money_ticket2	>	0	
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				where		amount	>	0	
			)

			union

			#プリペイド支払
			(
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'プリペイド支払'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		store_history_date			
					,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.receipt_money_ticket1,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.receipt_money_ticket1,0),0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "
						sl.del_flg	=	0	
						and		sl.receipt_money_ticket1	>	0	
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				where		amount	>	0	
			)
			
			union

			#売掛金（現金）
			(
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'売掛金（現金）'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		store_history_date			
					,		COALESCE(SUM(IF(sl.tran_div in(0,2,3),COALESCE(sl.karte_credit_sales,0),0))- SUM(IF(sl.tran_div=1,COALESCE(sl.karte_credit_sales,0),0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "	
						sl.del_flg	=	0	
						and		sl.karte_credit_sales	>	0	
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				where		amount	>	0	
			)
			
			union

			#技術売上合計
			(
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'技術売上合計'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		store_history_date			
					,		coalesce(sum(if(sl.tran_div IN(0,2,3),coalesce(sl.skill_tax_inc_sal_price_amount,0), 0))-sum(if(tran_div=1,coalesce(sl.skill_tax_inc_sal_price_amount,0), 0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "
						sl.del_flg	=	0	
						and		sl.skill_tax_inc_sal_price_amount	>	0	
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				where		amount	>	0	
			)

			union

			#店販売上合計
			(
				select 					
						A.store_cd	as	店舗コード	
				,		store_name	as	店舗名	
				,		''	as	部門コード	
				,		''	as	部門名	
				,		1	as	区分	
				,		store_history_date	as	売上年月日	
				,		'店販売上合計'	as	項目	
				,		''	as	クレジットコード	
				,		''	as	クレジット会社名	
				,		''	as	商品券コード	
				,		''	as	商品券名	
				,		''	as	入金・支払区分	
				,		''	as	入金・支払区分名	
				,		''	as	対象取引コード	
				,		''	as	請求先・支払先コード	
				,		''	as	請求先・支払先名	
				,		''	as	仕入先コード	
				,		''	as	仕入先名	
				,		''	as	項目コード	
				,		''	as	項目名	
				,		amount	as	金額	
				,		''	as	備考	
				from					
				(					
					select					
							sl.store_cd			
					,		store_history_date			
					,		coalesce(sum(if(sl.tran_div IN(0,2,3),coalesce(sl.sales_goods_tax_inc_sal_price_amount,0),0))-sum(if(tran_div=1,coalesce(sl.sales_goods_tax_inc_sal_price_amount,0),0)),0)	as	amount	
					from		d_sales	sl		
					where		"; /*sl.store_cd	=	@STORECD*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
						store_history_date >= " . $date_start ." 
						and store_history_date <= ". $date_end . "
						and ";
		}
		$sql .= "
						sl.del_flg	=	0	
						and		sl.sales_goods_tax_inc_sal_price_amount	>	0	
				) A					
				inner join		m_store	s		
				on		A.store_cd	=	s.store_cd	
				where		amount	>	0	
			)";

		if ($date_start == "" && $date_end == "") {
			$sql .= "
			order by 年月日 desc 
			limit 5 ";
		}
		$query = $this->db_vid->query($sql);
		return $query->result();
	}

	public function getInData($date_start, $date_end) {
		$sql = "
			select 				
						A.store_cd	as	店舗コード
				,		store_name	as	店舗名
				,		''	as	部門コード
				,		''	as	部門名
				,		2	as	区分
				,		business_day	as	年月日
				,		''	as	売上項目
				,		''	as	クレジットコード
				,		''	as	クレジット会社名
				,		''	as	商品券コード
				,		''	as	商品券名
				,		in_div	as	入金・支払区分
				,		div_name	as	入金・支払区分名
				,		tran_cd	as	対象取引コード
				,		A.receipt_dealer_cd	as	請求先・支払先コード
				,		coalesce(dl.receipt_dealer_name, '')	as	請求先・支払先名
				,		item_cd	as	項目コード
				,		subject_name	as	項目名
				,		''	as	仕入先コード
				,		''	as	仕入先名
				,		in_price	as	金額
				,		explanation	as	備考
			from				
			(				
				select				
						mn.store_cd		
				,		store_name		
				,		mn.business_day		
				,		in_div		
				,		case in_div when 1 then '現金預かり' when 2 then '入金先への請求' when 3 then '売掛回収金' end	as	div_name
				,		if(in_div in (1,3) , in_to, 0)	as	tran_cd
				,		if(in_div=2 , in_to, 0)	as	receipt_dealer_cd
								
				,		in_item	as	item_cd
				,		coalesce(receipt_subject_name,'')	as	subject_name
				,		in_price		
				,		explanation		
				from		d_in_money	mn	
				left join		m_receipt_subject	sb	
				on		mn.store_cd	=	sb.store_cd
				and		mn.in_item	=	sb.receipt_subject_cd
				inner join		m_store	s	
				on		mn.store_cd	=	s.store_cd
				where ";
					/*mn.store_cd	=	276
					and		in_div	in	(2,3)*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
					mn.business_day >= " . $date_start ." 
					and mn.business_day <= ". $date_end . "
					and ";
		}
		$sql .=	"
					in_method	=	3
					and		in_stat	=	1
					and		mn.del_flg	=	0 ";
		if ($date_start == "" && $date_end == "") {
			$sql .= "
				order by mn.business_day desc 
				limit 5 ";
		}
		$sql .= "
			)		A		
			left join		m_receipt_dealer	dl	
			on		A.store_cd	=	dl.store_cd
			and		A.receipt_dealer_cd	=	dl.receipt_dealer_cd";
		$query = $this->db_vid->query($sql);
		return $query->result();
	}

	public function getOutData($date_start, $date_end) {
		$sql = "
			select 				
						A.store_cd	as	店舗コード
				,		store_name	as	店舗名
				,		''	as	部門コード
				,		''	as	部門名
				,		3	as	区分
				,		business_day	as	年月日
				,		''	as	売上項目
				,		''	as	クレジットコード
				,		''	as	クレジット会社名
				,		''	as	商品券コード
				,		''	as	商品券名
				,		out_div	as	入金・支払区分
				,		div_name	as	入金・支払区分名
				,		tran_cd	as	対象取引コード
				,		A.payment_dealer_cd	as	請求先・支払先コード
				,		coalesce(dl.payment_dealer_name, '')	as	請求先・支払先名
				,		A.dealer_cd	as	仕入先コード
				,		coalesce(d.dealer_name, '')	as	仕入先名
				,		item_cd	as	項目コード
				,		subject_name	as	項目名
				,		out_price	as	金額
				,		explanation	as	備考
			from				
			(				
				select				
							mn.store_cd		
					,		store_name		
					,		mn.business_day		
					,		out_div		
					,		case out_div when 1 then 'クレジット・商品券釣銭' when 2 then '返品・返金' when 3 then '支払先への支払' when 4 then '仕入先への支払' when 5 then '売掛回収時釣銭' when 6 then '元伝票の売掛回収額（現金)' end	as	div_name
					,		if(out_div in (1,2,6) ,out_to, 0)	as	tran_cd
					,		if(out_div=3 ,out_to, 0)	as	payment_dealer_cd
					,		if(out_div=4 ,out_to, 0)	as	dealer_cd
					,		out_item	as	item_cd
					,		if(out_item=9999, '仕入支払', coalesce(payment_subject_name,''))	as	subject_name
					,		out_price		
					,		explanation		
				from		d_out_money	mn	
					left join		m_payment_subject	sb	
					on		mn.store_cd	=	sb.store_cd
					and		mn.out_item	=	sb.payment_subject_cd
					inner join		m_store	s	
					on		mn.store_cd	=	s.store_cd
				where		
					";
					/*mn.store_cd	=	276*/
		if ($date_start != "" && $date_end != "") {
			$sql .= "
					mn.business_day >= " . $date_start ." 
					and mn.business_day <= ". $date_end . "
					and ";
		}
		$sql .=	"
					out_div	<>	0
					and		out_method	=	3
					and		out_stat	=	1
					and		mn.del_flg	=	0 ";
		if ($date_start == "" && $date_end == "") {
			$sql .= "
				order by mn.business_day desc 
				limit 5 ";
		}
		$sql .= "
			)		A		
			left join		m_payment_dealer	dl	
			on		A.store_cd	=	dl.store_cd
			and		A.payment_dealer_cd	=	dl.payment_dealer_cd
			left join		m_dealer	d	
			on		A.store_cd	=	d.store_cd
			and		A.dealer_cd	=	d.dealer_cd";
		$query = $this->db_vid->query($sql);
		return $query->result();
	}
}

/* End of file vid_model.php */
/* Location: ./application/model/vid_model.php */