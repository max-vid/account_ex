<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conv_Model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	
	public function getAllFormat(){
		$query = $this->db->get('conv_format');
		return $query->result();
	}

	public function getMethodList($format_id){
		$this->db->where("format_id",$format_id);
		$query = $this->db->get('conv_method');
		return $query->result();
	}

	public function getDefaultMethod($format_id,$datatype){
		$this->db->where("format_id",$format_id);
		$this->db->where("datatype",$datatype);
		$this->db->where("default",1);
		$query = $this->db->get('conv_method');
		return $query->result()[0];
	}

	public function getMethodDetails($id){
		$this->db->where("id",$id);
		$query = $this->db->get('conv_method');
		return $query->result()[0];
	}

	public function createMethod($format_id,$name){
		$data = array('format_id' => $format_id, 'name' => $name);
		$this->db->insert('conv_method', $data);
		return $this->db->insert_id();
	}

	public function getMethodDetailsList($method_id){
		$this->db->where("method_id",$method_id);
		$query = $this->db->get('conv_method_details');
		return $query->result();
	}

	public function createMethodDetailsList($method_id){
		$method = $this->getMethodDetails($method_id);
		$column_list = $this->getColumnList($method->format_id);
		foreach($column_list as $column){
			$data = array('method_id' => $method_id, 'column_id' => $column->id);
			$this->db->insert('conv_method_details',$data);
		}
	}

	public function getMethodFieldSetting($id){
		$this->db->where("id",$id);
		$query = $this->db->get('conv_method_details');
		return $query->result()[0];
	}

	public function setMethodFieldSetting($id,$data){
		$this->db->where("id",$id);
		$this->db->update('conv_method_details',$data);
	}

	public function getColumnList($format_id){
		$this->db->where("format_id",$format_id);
		$this->db->order_by("id","asc");
		$query = $this->db->get('conv_column');
		return $query->result();
	}

	public function getColumnDetails($format_id, $column_name){
		$this->db->where("format_id",$format_id);
		$this->db->where("name",$column_name);
		$query = $this->db->get('conv_column');
		return $query->result();
	}

	public function getColumnName($column_id){
		$this->db->where("id",$column_id);
		$query = $this->db->get('conv_column');
		return $query->result()[0]->name;
	}

	public function getMapList($format_id,$column_id){
		$this->db->where("format_id",$format_id);
		$this->db->where("column_id",$column_id);
		$query = $this->db->get('conv_mapping');
		return $query->result();
	}

	public function mapData($format_id,$column_id,$source){
		$this->db->where("format_id",$format_id);
		$this->db->where("column_id",$column_id);
		$this->db->where("source",$source);
		$query = $this->db->get('conv_mapping');
		return $query->result();
	}
}

/* End of file conv_model.php */
/* Location: ./application/model/conv_model.php */