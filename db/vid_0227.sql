-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2015 at 04:15 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vid_edit_bs`
--

-- --------------------------------------------------------

--
-- Table structure for table `conv_column`
--

CREATE TABLE IF NOT EXISTS `conv_column` (
`id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `description` varchar(35) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=sjis AUTO_INCREMENT=29 ;

--
-- Dumping data for table `conv_column`
--

INSERT INTO `conv_column` (`id`, `format_id`, `name`, `description`) VALUES
(1, 1, '識別フラグ', 'Identification flag'),
(2, 1, '伝票No', 'No slip'),
(3, 1, '決算', 'Closing'),
(4, 1, '取引日付', 'Transaction date'),
(5, 1, '借方勘定科', 'Debit account family'),
(6, 1, '目借方補助項', 'Eye debit auxiliary section'),
(7, 1, '目借方部門', 'Eye debit department'),
(8, 1, '借方税区', 'Debit tax-ku'),
(9, 1, '借方金額', 'Debit amount'),
(10, 1, '借方税金', 'Debit tax'),
(11, 1, '金額貸方勘', 'Amount credited intuition'),
(12, 1, '定科目', 'Constant subjects'),
(13, 1, '助科目 貸方部門', 'Assistant courses credit department'),
(14, 1, '貸方税区', 'Credit tax-ku'),
(15, 1, '貸方金', 'Credit money'),
(16, 1, '貸方税', 'Credit tax'),
(17, 1, '額適', 'Gakuteki'),
(18, 1, '番号', 'Number'),
(19, 1, '期日', 'Date'),
(20, 1, 'タイプ', 'Type'),
(21, 1, '生成元', 'Generator'),
(22, 1, '仕訳メモ', 'Journal notes'),
(23, 1, '付箋１', 'Sticky 1'),
(24, 1, '付箋２', 'Sticky 2'),
(25, 1, '調整', 'Adjustment'),
(26, 2, 'atest', 'test'),
(27, 2, 'asdfadsf', 'test'),
(28, 2, 'btest', 'astre');

-- --------------------------------------------------------

--
-- Table structure for table `conv_conditional`
--

CREATE TABLE IF NOT EXISTS `conv_conditional` (
`id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `priority` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=sjis AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conv_format`
--

CREATE TABLE IF NOT EXISTS `conv_format` (
`id` int(11) NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=sjis AUTO_INCREMENT=3 ;

--
-- Dumping data for table `conv_format`
--

INSERT INTO `conv_format` (`id`, `name`) VALUES
(1, '弥生会計'),
(2, 'XX会計');

-- --------------------------------------------------------

--
-- Table structure for table `conv_mapping`
--

CREATE TABLE IF NOT EXISTS `conv_mapping` (
`id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `source` varchar(25) CHARACTER SET utf8 NOT NULL,
  `explanation_text` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `destination` varchar(25) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=sjis AUTO_INCREMENT=15 ;

--
-- Dumping data for table `conv_mapping`
--

INSERT INTO `conv_mapping` (`id`, `format_id`, `column_id`, `source`, `explanation_text`, `destination`) VALUES
(1, 1, 5, '1001', '売上高', '102001'),
(2, 1, 5, '1002', '商品売上高', '102002'),
(3, 1, 5, '1003', 'サービス料', '102004'),
(4, 1, 1, '2100', NULL, '200'),
(5, 1, 25, '2110', NULL, '200'),
(6, 1, 2, '*', 'default value', '9999'),
(7, 1, 2, '2110', NULL, '200'),
(8, 1, 4, '2110', NULL, '200'),
(9, 1, 4, '*', NULL, '9999'),
(10, 1, 4, '*', NULL, '800'),
(11, 1, 3, '*', NULL, 'B'),
(12, 1, 1, '2101', NULL, '201'),
(14, 1, 1, '999', NULL, '209');

-- --------------------------------------------------------

--
-- Table structure for table `conv_method`
--

CREATE TABLE IF NOT EXISTS `conv_method` (
`id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 NOT NULL,
  `datatype` int(1) DEFAULT NULL COMMENT '1:売上;2:入金;3:出金;',
  `default` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=sjis AUTO_INCREMENT=7 ;

--
-- Dumping data for table `conv_method`
--

INSERT INTO `conv_method` (`id`, `format_id`, `name`, `description`, `datatype`, `default`) VALUES
(1, 1, 'VIDから弥生会計1 ', 'Yayoi accounting from VID 1', 1, 1),
(2, 1, 'VIDから弥生会計2', 'Yayoi accounting from VID 2', 2, 0),
(3, 1, 'VID 3', '', 3, 0),
(4, 1, 'VID 4', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `conv_method_details`
--

CREATE TABLE IF NOT EXISTS `conv_method_details` (
`id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `field_setting` int(1) NOT NULL,
  `field_setting_details` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=sjis AUTO_INCREMENT=151 ;

--
-- Dumping data for table `conv_method_details`
--

INSERT INTO `conv_method_details` (`id`, `method_id`, `column_id`, `field_setting`, `field_setting_details`) VALUES
(1, 1, 1, 2, '1:店舗コード:1'),
(2, 1, 2, 2, '5:区分:1'),
(3, 1, 3, 2, '2:店舗名:0'),
(4, 1, 4, 2, '6:年月日:0'),
(5, 1, 5, 2, '7:売上項目:0'),
(6, 1, 6, 1, 'yyyy-mm-dd'),
(7, 1, 7, 1, 'yyyy-mm-dd'),
(8, 1, 8, 2, '5:区分:0'),
(9, 1, 9, 2, '11:商品券名:0'),
(10, 1, 10, 0, ''),
(11, 1, 11, 0, ''),
(12, 1, 12, 0, ''),
(13, 1, 13, 0, ''),
(14, 1, 14, 0, ''),
(15, 1, 15, 0, ''),
(16, 1, 16, 0, ''),
(17, 1, 17, 0, ''),
(18, 1, 18, 0, ''),
(19, 1, 19, 0, ''),
(20, 1, 20, 0, ''),
(21, 1, 21, 0, ''),
(22, 1, 22, 0, ''),
(23, 1, 23, 0, ''),
(24, 1, 24, 0, ''),
(25, 1, 25, 0, ''),
(26, 3, 1, 2, '1:識別フラグ:0'),
(27, 3, 2, 0, ''),
(28, 3, 3, 0, ''),
(29, 3, 4, 0, ''),
(30, 3, 5, 0, ''),
(31, 3, 6, 0, ''),
(32, 3, 7, 0, ''),
(33, 3, 8, 0, ''),
(34, 3, 9, 0, ''),
(35, 3, 10, 0, ''),
(36, 3, 11, 0, ''),
(37, 3, 12, 0, ''),
(38, 3, 13, 0, ''),
(39, 3, 14, 0, ''),
(40, 3, 15, 0, ''),
(41, 3, 16, 0, ''),
(42, 3, 17, 0, ''),
(43, 3, 18, 0, ''),
(44, 3, 19, 0, ''),
(45, 3, 20, 0, ''),
(46, 3, 21, 0, ''),
(47, 3, 22, 0, ''),
(48, 3, 23, 0, ''),
(49, 3, 24, 0, ''),
(50, 3, 25, 0, ''),
(51, 4, 1, 2, '1:識別フラグ:1'),
(52, 4, 2, 2, '2:伝票No:1'),
(53, 4, 3, 0, ''),
(54, 4, 4, 0, ''),
(55, 4, 5, 0, ''),
(56, 4, 6, 0, ''),
(57, 4, 7, 0, ''),
(58, 4, 8, 0, ''),
(59, 4, 9, 0, ''),
(60, 4, 10, 0, ''),
(61, 4, 11, 0, ''),
(62, 4, 12, 0, ''),
(63, 4, 13, 0, ''),
(64, 4, 14, 0, ''),
(65, 4, 15, 0, ''),
(66, 4, 16, 0, ''),
(67, 4, 17, 0, ''),
(68, 4, 18, 0, ''),
(69, 4, 19, 0, ''),
(70, 4, 20, 0, ''),
(71, 4, 21, 0, ''),
(72, 4, 22, 0, ''),
(73, 4, 23, 0, ''),
(74, 4, 24, 0, ''),
(75, 4, 25, 0, ''),
(76, 2, 1, 0, ''),
(77, 2, 2, 0, ''),
(78, 2, 3, 0, ''),
(79, 2, 4, 0, ''),
(80, 2, 5, 0, ''),
(81, 2, 6, 0, ''),
(82, 2, 7, 0, ''),
(83, 2, 8, 0, ''),
(84, 2, 9, 0, ''),
(85, 2, 10, 0, ''),
(86, 2, 11, 0, ''),
(87, 2, 12, 0, ''),
(88, 2, 13, 0, ''),
(89, 2, 14, 0, ''),
(90, 2, 15, 0, ''),
(91, 2, 16, 0, ''),
(92, 2, 17, 0, ''),
(93, 2, 18, 0, ''),
(94, 2, 19, 0, ''),
(95, 2, 20, 0, ''),
(96, 2, 21, 0, ''),
(97, 2, 22, 0, ''),
(98, 2, 23, 0, ''),
(99, 2, 24, 0, ''),
(100, 2, 25, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conv_column`
--
ALTER TABLE `conv_column`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_conditional`
--
ALTER TABLE `conv_conditional`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_format`
--
ALTER TABLE `conv_format`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_mapping`
--
ALTER TABLE `conv_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_method`
--
ALTER TABLE `conv_method`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conv_method_details`
--
ALTER TABLE `conv_method_details`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conv_column`
--
ALTER TABLE `conv_column`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `conv_conditional`
--
ALTER TABLE `conv_conditional`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `conv_format`
--
ALTER TABLE `conv_format`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `conv_mapping`
--
ALTER TABLE `conv_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `conv_method`
--
ALTER TABLE `conv_method`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `conv_method_details`
--
ALTER TABLE `conv_method_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
